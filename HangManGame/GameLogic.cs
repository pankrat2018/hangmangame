﻿using System.Linq;

namespace HangManGame
{
   public class GameLogic
    {
        public static char[] CheckWord(char playerGuess,char[] guess, string mysteryWord)
        {
                for (int i = 0; i < mysteryWord.Length; i++)
                {
                    if (playerGuess == mysteryWord[i])
                        guess[i] = playerGuess;
                }
            if (new string(guess) == mysteryWord)
                return "win".ToArray();
            else return guess;
        }
    }
}
