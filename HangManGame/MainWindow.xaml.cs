﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Input;


namespace HangManGame
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        string _word = ""; string _guess = "";
        private void btnPlay_Click(object sender, RoutedEventArgs e)
        {
            string btnText = btnPlay.Content.ToString();
            if (btnText == "Начать игру")
            {
                btnPlay.Content = "Проверить символ";
                textBoxSymbolWord.IsEnabled = true;
                btnText = btnPlay.Content.ToString();
                if (radioButtonSport.IsChecked.Value)
                {
                    GetInfo("sport", ref _word, ref _guess);
                    textBlockWord.Text = _guess;
                }
                if (radioButtonHighTech.IsChecked.Value)
                {
                    GetInfo("hightech", ref _word, ref _guess);
                    textBlockWord.Text = _guess;
                }
                if (radioButtonArt.IsChecked.Value)
                {
                    GetInfo("art", ref _word, ref _guess);
                    textBlockWord.Text = _guess;
                }
                radioButtonArt.IsEnabled = false;
                radioButtonHighTech.IsEnabled = false;
                radioButtonSport.IsEnabled = false;

            }
            else if (btnText == "Проверить символ")
            {
                if(textBoxSymbolWord.Text == "")
                {
                    MessageBox.Show("Введите символ", "", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                char[] checkArray = GameLogic.CheckWord(Convert.ToChar(textBoxSymbolWord.Text.ToLower()), _guess.ToArray(), _word);
                _guess = new string(checkArray);
                textBlockWord.Text = _guess;
                if (_guess == "win")
                    if (MessageBox.Show("Вы угадали слово, желаете начать заново?", "Поздравляем", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        radioButtonArt.IsEnabled = true;
                        radioButtonHighTech.IsEnabled = true;
                        radioButtonSport.IsEnabled = true;
                        btnPlay.Content = "Начать игру";
                        btnText = btnPlay.Content.ToString();
                        textBlockWord.Text = "";
                        textBoxSymbolWord.IsEnabled = false;
                    }

                    else
                    {
                        System.Diagnostics.Process.GetCurrentProcess().Kill();
                    }
            }
            textBoxSymbolWord.Text = "";
            textBoxSymbolWord.Focus();

        }

        private void textBoxSymbolWord_KeyDown(object sender, KeyEventArgs e)
        {
            if (textBoxSymbolWord.Text.Length >= 1)
            {
                e.Handled = true;
            }
        }
        private static void GetInfo(string baseName, ref string word, ref string guess)
        {
            word = Words.GetMysteryWord(baseName);
            if(word != "error base")
            guess = Words.EncodeMysteryWord(word);
            else
            {
                MessageBox.Show("Не найдена база слов, восстановите файл с базой и перезапустите программу", "Произошла ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                System.Diagnostics.Process.GetCurrentProcess().Kill();
            }
        }
    }

}
