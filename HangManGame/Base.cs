﻿using System.IO;

namespace HangManGame
{
   public class Base
    {
        public string[] GetBase(string name)
        {
            if(File.Exists(name + ".base"))
            {
                return File.ReadAllLines(name + ".base");
            }
            else
            {
                return null;
            }
            
        }
       /* private string[] EncodeBase()
        {
            
        }*/
    }
}
