﻿using System;

namespace HangManGame
{
   public class Words
    {
        public static string GetMysteryWord(string theme)
        {
            Base baze = new Base();
            string[] words = baze.GetBase(theme);
            if (words != null)
            {
                Random randGen = new Random();
                int index = randGen.Next(0, words.Length);
                return words[index];
            }
            else return "error base";
            

        }
        public static string EncodeMysteryWord(string mysteryWord)
        {
            char[] guess = new char[mysteryWord.Length]; 
            for (int i = 0; i < mysteryWord.Length; i++)
            {
                guess[i] = '*';
            }
            string word = new string(guess);
            return word;
        }
    }
}
