﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using HangManGame;

namespace Tests
{
    [TestClass]
    public class UnitTests
    {
        [TestMethod]
        public void TestEncodeWords()
        {
            Assert.AreEqual(Words.EncodeMysteryWord("компьютер"), "*********");
        }

        [TestMethod]
        public void TestGameLogic()
        {
            char[] guess = new char[] { '*', '*', '*' };
            string result = new string(GameLogic.CheckWord('о',guess,"гол"));
            Assert.AreEqual(result,"*о*");
        }
    }
}
